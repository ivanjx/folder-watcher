package main;

import "fmt";
import "os";
import "runtime";
import "path/filepath";
import "github.com/fsnotify/fsnotify";

func stringSliceIndexOf(slice *[]string, value *string) (int) {
	for i := 0; i < len(*slice); i++ {
		if (*slice)[i] == *value {
			return i;
		}
	}

	return -1;
}

func main() {
	// Getting cwd.
	cwd, _ := filepath.Abs(filepath.Dir(os.Args[0])); // Args[0] contains full path of the exe.
	args := os.Args[1:];

	// Some fancy messages.
	fmt.Printf("=== FOLDER ACTIVITY WATCHER ===\n");
	fmt.Printf("By: Ivan Kara\n");
	fmt.Printf("===============================\n");

	// Getting target folder.
	if len(args) != 1 {
		fmt.Printf("Wrong parameters.\nUsage: <executable> <target_folder>\n");
		os.Exit(1);
	}

	targetFolder := args[0];
	targetFolder, _ = filepath.Abs(targetFolder);

	// Getting log path.
	logPath := filepath.Join(cwd, "watcher-log.txt");
	logFileName := filepath.Base(logPath);

	// Preparing watcher.
	fmt.Printf("Initializing...\n");
	watcher, err := fsnotify.NewWatcher();

	if err != nil {
		fmt.Printf("Error initializing watcher: %s\n", err.Error());
		os.Exit(1);
	}

	// Adding current folder and its subfolders.
	var watchList []string;

	filepath.Walk(
		targetFolder,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				fmt.Printf("Error listing directory: %s\n", err.Error());
				return nil;
			}

			if info.IsDir() {
				watchList = append(watchList, path);
				watcher.Add(path);
				fmt.Printf("Adding |%s| to watch list...\n", path);
			}

			return nil;
		},
	);

	// Opening log file.
	logFs, err := os.OpenFile(logPath, os.O_CREATE | os.O_WRONLY, 0755);

	if err != nil {
		fmt.Printf("Error opening log file: %s\n", err.Error());
		os.Exit(1);
	}

	// Warning message.
	if runtime.GOOS == "windows" {
		fmt.Printf("WARNING: Recursive watcher has some issues with renaming folder on Windows systems.\n");
	}

	// Start listening.
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return;
				}
	
				canPrint := true;
	
				if event.Op == fsnotify.Chmod {
					// Ignore all abt chmod.
					canPrint = false;
				} else {
					// Dont get file info on remove or rename.
					if event.Op != fsnotify.Remove && event.Op != fsnotify.Rename {
						// Getting file stat.
						stat, err := os.Stat(event.Name);
		
						if err != nil {
							fmt.Printf("Unable to get file stat: %s\n", err.Error());
							canPrint = false;
						} else {
							if stat.IsDir() {
								// Dont print if directory.
								canPrint = false;
	
								if stringSliceIndexOf(&watchList, &(event.Name)) == -1 {
									// Adding to the watcher.
									fmt.Printf("Adding |%s| to watch list...\n", event.Name);
									watcher.Add(event.Name);
									watchList = append(watchList, event.Name);
								}
							}
						}
					} else { // Remove folder on remove or rename.
						i := stringSliceIndexOf(&watchList, &(event.Name));
	
						if i != -1 {
							// Removing from watch list.
							fmt.Printf("Removing |%s| from watch list...\n", event.Name);
							watcher.Remove(event.Name);
							watchList = append(watchList[:i], watchList[i+1:]...);
						}
					}
				}

				// Dont print log file.
				fileName := filepath.Base(event.Name);

				if fileName == logFileName {
					canPrint = false;
				}
	
				if canPrint {
					log := fmt.Sprintf("%s: %s\n", event.Op, event.Name);
					fmt.Printf(log);
					logFs.WriteString(log);
				}
	
			case err, ok := <-watcher.Errors:
				if !ok {
					return;
				}
	
				fmt.Printf("Error received: %s\n", err.Error());
			}
		}
	}();

	fmt.Printf("Watching |%s| and its subfolders...\n", targetFolder);
	fmt.Printf("Press ENTER key to end the process appropriately!\n");

	// Wait till enter key pressed.
	fmt.Scanln();
	
	// Done called.
	fmt.Printf("Releasing resources...\n");

	// Releasing watchers.
	watcher.Close();

	// Releasing log file.
	logFs.Close();

	// End.
	fmt.Print("Thanks for using this program :)\n");
}